module.exports = {
  env: {
    // 当前的配置为根配置
    // root:true,
    browser: true,
    es2021: true,
    "vue/setup-compiler-macros": true,
  },
  extends: [
    "eslint-config-prettier",
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:vue/vue3-recommended",
    "plugin:prettier/recommended",
    "standard",
    "prettier",
  ],
  overrides: [],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    parser: "@typescript-eslint/parser",
  },
  parser: "vue-eslint-parser",
  plugins: ["vue", "@typescript-eslint", "prettier"],
  rules: {
    quotes: ["error", "double"],
    "comma-dangle": "off",
    "vue/multi-word-component-names": "off",
    "@typescript-eslint/no-var-requires": "off",
    "no-unused-vars": "off",
    "prefer-const": "off",
    "vue/attribute-hyphenation": "off",
    "jsx-quotes": ["error", "prefer-double"],
    "vue/no-v-html": "off",
    "vue/no-ref-as-operand": "off",
    "vue/v-on-event-hyphenation": "off",
  },
};
/*
--prettier与eslint冲突解决--
*/
// *eslint-config-prettier  解决eslint中样式规范与prettier冲突时以prettier为主
