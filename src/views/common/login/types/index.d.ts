export interface dataToParam {
  userName: string;
  password: string;
  uuid: string;
  captcha: string;
}
