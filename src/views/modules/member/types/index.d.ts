export interface memberType {
  birthDate: string;
  loginPassword: string;
  modifyTime: string;
  nickName: string;
  payPassword: string;
  pic: string;
  realName: string;
  score: number;
  sex: string;
  status: number;
  userId: string;
  userLastip: string;
  userLasttime: string;
  userMail: string;
  userMemo: string;
  userMobile: string;
  userRegip: string;
  userRegtime: string;
}
export interface editMemberType {
  nickName: string;
  status: number;
  pic: string | null;
  userId: string;
}
export interface memberResType {
  current: number;
  pages: number;
  records: memberType[];
  searchCount: boolean;
  size: number;
  total: number;
}
