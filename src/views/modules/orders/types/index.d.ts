import { tableType } from "@/types";
export interface ProdReq extends tableType {
  content?: string;
  status?: number;
  title?: string;
}
export interface orderItem {
  basketDate: string;
  commSts: number;
  distributionCardNo: string;
  orderItemId: number;
  orderNumber: string;
  pic: string;
  price: number;
  prodCount: number;
  prodId: number;
  prodName: string;
  productTotalAmount: number;
  recTime: string;
  shopId: number;
  skuId: number;
  skuName: string;
  userId: string;
}
export interface userOrder {
  addr: string;
  addrId: number;
  addrOrderId: number;
  area: string;
  areaId: number;
  city: string;
  cityId: number;
  createTime: string;
  mobile: string;
  postCode: string;
  province: string;
  provinceId: number;
  receiver: string;
  userId: number;
  version: string;
}
export interface orderType {
  actualTotal?: number;
  addrOrderId?: number;
  cancelTime?: string;
  createTime?: string;
  deleteStatus?: number;
  dvyFlowId?: number;
  dvyId?: string;
  dvyTime?: string;
  dvyType?: string;
  finallyTime?: string;
  freightAmount?: number;
  isPayed?: number;
  orderId?: number;
  orderItems: orderItem[];
  orderNumber?: string;
  payTime?: string;
  payType?: number;
  prodName?: string;
  productNums?: number;
  reduceAmount?: number;
  refundSts?: number;
  remarks?: string;
  shopId?: number;
  shopName?: string;
  status?: number;
  total?: number;
  updateTime?: string;
  userAddrOrder?: userOrder[];
  userId?: string;
}
export interface orderResType {
  current: number;
  pages: number;
  records: orderType[];
  searchCount: boolean;
  size: number;
  total: number;
}
export interface pageType {
  current: number;
  size: number;
}
