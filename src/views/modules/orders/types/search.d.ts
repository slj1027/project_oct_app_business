export interface paramsSearch {
  current?: number;
  size?: number;
  orderNumber?: string;
  status?: number | null;
  startTime?: string;
  endTime?: string;
}
