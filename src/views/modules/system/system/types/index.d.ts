export interface systemType {
  id: number;
  time: number;
  createDate: string;
  ip: string;
  method: string;
  operation: string;
  params: string;
  username: string;
}
export interface systemResType {
  current: number;
  pages: number;
  records: systemType[];
  searchCount: boolean;
  size: number;
  total: number;
}
