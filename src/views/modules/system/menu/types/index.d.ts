/* 
icon: null
list: null
menuId: 7
name: "查看"
orderNum: 0
parentId: 6
parentName: null
perms: "sys:schedule:page,sys:schedule:info"
type: 2
url: null

*/

export interface MenuType {
  icon?: string;
  list: any[];
  menuId: number;
  name: string;
  orderNum: number;
  parentId: number;
  parentName: string;
  perms: string | string;
  type: string | number;
  url: string;
  children?: MenuType[];
}

export interface editMenuType {
  icon: string;
  menuId: number;
  name: string;
  orderNum: number;
  parentId: number;
  perms: string;
  type: number;
  url: string;
}
export interface MenuResType {
  current: number;
  pages: number;
  records: MenuType[];
  searchCount: boolean;
  size: number;
  total: number;
}
