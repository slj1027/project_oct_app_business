export interface configType {
  id?: number;
  paramKey: string;
  paramValue: string;
  remark: string;
}
export interface configResType {
  current: number;
  pages: number;
  records: configType[];
  searchCount: boolean;
  size: number;
  total: number;
}
export interface configReqType {
  current: number;
  size: number;
  paramKey?: string;
}
