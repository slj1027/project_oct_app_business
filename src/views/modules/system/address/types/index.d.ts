/* 
{
	"current": 0,
	"pages": 0,
	"records": [
		{
			"areaId": 0,
			"areaName": "",
			"areas": [
				{
					"areaId": 0,
					"areaName": "",
					"areas": [
						{}
					],
					"level": 0,
					"parentId": 0
				}
			],
			"level": 0,
			"parentId": 0
		}
	],
	"searchCount": true,
	"size": 0,
	"total": 0
}
*/
export interface AreaType {
  areaId: number;
  areaName: string;
  areas: any[];
  level: number;
  parentId: number;
}
export interface AddressType {
  areaId: number;
  areaName: string;
  areas?: AreaType[];
  level?: number;
  parentId?: number | null;
}
export interface AddressResType {
  current: number;
  pages: number;
  records: AddressType[];
  searchCount: boolean;
  size: number;
  total: number;
}
