export interface roleType {
  roleId: number;
  roleName: string;
  createTime: string;
  remark: string;
  menuIdList: any[] | null;
}
export interface roleResType {
  current: number;
  pages: number;
  records: roleType[];
  searchCount: boolean;
  size: number;
  total: number;
}
export interface editRoleType {
  roleId?: number;
  roleName: string;
  remark: string;
  menuIdList: number[] | null;
}
