export interface adminType {
  createTime: string;
  email: string;
  mobile: string;
  roleIdList: number[] | null;
  shopId: number;
  userId: number;
  status: number;
  username: string;
  password?: string;
  surePsw?: string;
}
export interface adminResType {
  current: number;
  pages: number;
  records: adminType[];
  searchCount: boolean;
  size: number;
  total: number;
}
export interface editAdminType {
  email: string;
  mobile: string;
  roleIdList: number[] | null;
  userId?: number;
  status: number;
  username: string;
  password?: string;
  shopId?: number;
  surePsw?: string;
}
export interface AdminReqType {
  current: number;
  size: number;
  username?: string;
}
