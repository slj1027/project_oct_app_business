export interface editScheduleType {
  beanName: string;
  cronExpression: string;
  jobId?: number;
  methodName: string;
  params: string;
  remark: string;
}
export interface ScheduleType {
  beanName: string;
  createTime: string;
  cronExpression: string;
  jobId: number;
  methodName: string;
  params: string;
  remark: string;
  status: number;
}
export interface ScheduleResType {
  current: number;
  pages: number;
  records: ScheduleType[];
  searchCount: boolean;
  size: number;
  total: number;
}
export interface ScheduleLogType {
  beanName: string;
  createTime: string;
  error: string;
  jobId: number;
  logId: number;
  methodName: string;
  params: string;
  status: number;
  times: number;
}
