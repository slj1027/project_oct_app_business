import { tableType } from "@/types";
export interface GroupingReqType extends tableType {
  title?: string;
  status?: number;
}
export interface GroupingType {
  createTime?: string;
  deleteTime?: string;
  id?: number;
  isDefault?: number;
  prodCount?: number;
  seq: number;
  shopId?: number;
  status: number;
  style: number;
  title: string;
  updateTime?: string;
}

export interface GroupingResType {
  current: number;
  pages: number;
  records: GroupingType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": 0,
	"pages": 0,
	"records": [
		{
			"createTime": "",
			"deleteTime": "",
			"id": 0,
			"isDefault": 0,
			"prodCount": 0,
			"seq": 0,
			"shopId": 0,
			"status": 0,
			"style": 0,
			"title": "",
			"updateTime": ""
		}
	],
	"searchCount": true,
	"size": 0,
	"total": 0
}

*/
