export interface UserType {
  nickName: string;
  pic: string;
  userId: string;
  userMobile: string;
}

export interface CommentType {
  content: string;
  evaluate: number;
  isAnonymous: number;
  orderItemId: number;
  pics: string;
  postip: string;
  prodCommId: number;
  prodId: number;
  prodName: string;
  recTime: string;
  replyContent: string;
  replySts: number;
  replyTime: string;
  score: number;
  status: number;
  usefulCounts: number;
  user: UserType;
  userId: string;
}

export interface CommentResType {
  current: number;
  pages: number;
  records: CommentType[];
  searchCount: true;
  size: number;
  total: number;
}
