export interface BrandsType {
  brandId: number;
  brandName: string;
  brandPic: string;
  brief: string;
  content: string;
  firstChar: string;
  memo: string;
  recTime: string;
  seq: number;
  status: number;
  updateTime: string;
  userId: string;
}
export interface CategoriesType {
  attributeIds: [];
  brandIds: [];
  brands: [];
  categories: [];
  categoryId: number;
  categoryName: string;
  grade: number;
  icon: string;
  parentId: number;
  pic: string;
  prodProps: [];
  products: [];
  recTime: string;
  seq: number;
  shopId: number;
  status: number;
  updateTime: string;
}
export interface ProdPropsValues {
  propId: number;
  propValue: string;
  valueId: number;
}
export interface ProdPropsType {
  prodPropValues: ProdPropsValues[];
  propId: number;
  propName: string;
  rule: number;
  shopId: number;
}
export interface SkuListType {
  actualStocks: number;
  isDelete: number;
  modelId: string;
  oriPrice: number;
  partyCode: string;
  pic: string;
  price: number;
  prodId: number;
  prodName: string;
  properties: string;
  recTime: string;
  skuId: number;
  skuName: string;
  status: number;
  stocks: number;
  updateTime: string;
  version: number;
  volume: number;
  weight: number;
}
export interface ProductsType {
  brief: string;
  categoryId: number;
  content: string;
  createTime: string;
  deliveryMode: string;
  deliveryTemplateId: number;
  imgs: string;
  oriPrice: number;
  pic: string;
  price: number;
  prodId: number;
  prodName: string;
  putawayTime: string;
  shopId: number;
  shopName: string;
  skuList: SkuListType[];
  soldNum: number;
  status: number;
  tagList: any[];
  totalStocks: number;
  updateTime: string;
  version: number;
}
export interface CategoryResType {
  attributeIds?: any[];
  brandIds?: any[];
  brands?: BrandsType[];
  categories?: CategoriesType[];
  categoryId?: number;
  categoryName: string;
  grade?: number;
  icon?: string;
  parentId: number;
  pic: string;
  prodProps?: ProdPropsType[];
  products?: ProductsType[];
  recTime?: string;
  seq: number;
  shopId?: number;
  status: number;
  updateTime?: string;
}

/* 
[
	{
		"attributeIds": [],
		"brandIds": [],
		"brands": [
			{
				"brandId": number,
				"brandName": string,
				"brandPic": string,
				"brief": string,
				"content": string,
				"firstChar": string,
				"memo": string,
				"recTime": string,
				"seq": number,
				"status": number,
				"updateTime": string,
				"userId": string
			}
		],
		"categories": [
			{
				"attributeIds": [],
				"brandIds": [],
				"brands": [],
				"categories": [],
				"categoryId": number,
				"categoryName": string,
				"grade": number,
				"icon": string,
				"parentId": number,
				"pic": string,
				"prodProps": [],
				"products": [],
				"recTime": string,
				"seq": number,
				"shopId": number,
				"status": number,
				"updateTime": string
			}
		],
		"categoryId": number,
		"categoryName": string,
		"grade": number,
		"icon": string,
		"parentId": number,
		"pic": string,
		"prodProps": [
			{
				"prodPropValues": [
					{
						"propId": number,
						"propValue": string,
						"valueId": number
					}
				],
				"propId": number,
				"propName": string,
				"rule": number,
				"shopId": number
			}
		],
		"products": [
			{
				"brief": string,
				"categoryId": number,
				"content": string,
				"createTime": string,
				"deliveryMode": string,
				"deliveryTemplateId": number,
				"imgs": string,
				"oriPrice": number,
				"pic": string,
				"price": number,
				"prodId": number,
				"prodName": string,
				"putawayTime": string,
				"shopId": number,
				"shopName": string,
				"skuList": [
					{
						"actualStocks": number,
						"isDelete": number,
						"modelId": string,
						"oriPrice": number,
						"partyCode": string,
						"pic": string,
						"price": number,
						"prodId": number,
						"prodName": string,
						"properties": string,
						"recTime": string,
						"skuId": number,
						"skuName": string,
						"status": number,
						"stocks": number,
						"updateTime": string,
						"version": number,
						"volume": number,
						"weight": number
					}
				],
				"soldNum": number,
				"status": number,
				"tagList": [],
				"totalStocks": number,
				"updateTime": string,
				"version": number
			}
		],
		"recTime": string,
		"seq": number,
		"shopId": number,
		"status": number,
		"updateTime": string
	}
]
*/
