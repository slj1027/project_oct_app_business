import { tableType } from "@/types";
export interface ProdReq extends tableType {
  content?: string;
  status?: number;
  title?: string;
}
export interface ProdSku {
  actualStocks: number;
  isDelete: number;
  modelId: string;
  oriPrice: number;
  partyCode: string;
  pic: string;
  price: number;
  prodId: number;
  prodName: string;
  properties: string;
  recTime: string;
  skuId: number;
  skuName: string;
  status: number;
  stocks: number;
  updateTime: string;
  version: number;
  volume: number;
  weight: number;
}
export interface ProdType {
  brief?: string;
  categoryId?: number;
  content?: string;
  createTime?: string;
  deliveryMode?: string;
  deliveryTemplateId?: number;
  imgs?: string;
  oriPrice?: number;
  pic?: string;
  price?: number;
  prodId?: number;
  prodName?: string;
  putawayTime?: string;
  shopId?: number;
  shopName?: string;
  skuList?: ProdSku[];
  soldNum?: number;
  status?: number;
  tagList?: [];
  totalStocks?: number;
  updateTime?: string;
  version?: number;
}

export interface ProdResType {
  current: number;
  pages: number;
  records: ProdType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": number,
	"pages": number,
	"records": [
		{
			"brief": string,
			"categoryId": number,
			"content": string,
			"createTime": string,
			"deliveryMode": string,
			"deliveryTemplateId": number,
			"imgs": string,
			"oriPrice": number,
			"pic": string,
			"price": number,
			"prodId": number,
			"prodName": string,
			"putawayTime": string,
			"shopId": number,
			"shopName": string,
			"skuList": [
				{
					"actualStocks": number,
					"isDelete": number,
					"modelId": string,
					"oriPrice": number,
					"partyCode": string,
					"pic": string,
					"price": number,
					"prodId": number,
					"prodName": string,
					"properties": string,
					"recTime": string,
					"skuId": number,
					"skuName": string,
					"status": number,
					"stocks": number,
					"updateTime": string,
					"version": number,
					"volume": number,
					"weight": number
				}
			],
			"soldNum": number,
			"status": number,
			"tagList": [],
			"totalStocks": number,
			"updateTime": string,
			"version": number
		}
	],
	"searchCount": true,
	"size": number,
	"total": number
}
*/
