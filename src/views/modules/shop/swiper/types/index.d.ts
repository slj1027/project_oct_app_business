import { tableType } from "@/types";
export interface SwiperReq extends tableType {
  status?: number;
}

export interface SwiperType {
  des?: string;
  imgId?: number;
  imgUrl?: string;
  link?: string;
  pic: string;
  prodName?: string;
  relation?: number;
  seq: number;
  shopId?: number;
  status: number;
  title?: string;
  type: number;
  uploadTime?: string;
}
export interface SwiperResType {
  current: number;
  pages: number;
  records: SwiperType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": 0,
	"pages": 0,
	"records": [
		{
			"des": "",
			"imgId": 0,
			"imgUrl": "",
			"link": "",
			"pic": "",
			"prodName": "",
			"relation": 0,
			"seq": 0,
			"shopId": 0,
			"status": 0,
			"title": "",
			"type": 0,
			"uploadTime": ""
		}
	],
	"searchCount": true,
	"size": 0,
	"total": 0
}
*/
