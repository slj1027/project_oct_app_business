import { tableType } from "@/types";
export interface FreightReq extends tableType {
  content?: string;
  status?: number;
  title?: string;
}
export interface cityListType {
  areaId: number;
  areaName: string;
  areas: [
    {
      areaId: number;
      areaName: string;
      areas: any[];
      level: number;
      parentId: number;
    }
  ];
  level: number;
  parentId: number;
}
export interface transfeeFreesType {
  amount: number;
  freeCityList: cityListType[];
  freeType: number;
  piece: number;
  transfeeFreeId: number;
  transportId: number;
}

export interface transfeesType {
  cityList: cityListType[];
  continuousFee: number;
  continuousPiece: number;
  firstFee: number;
  firstPiece: number;
  transfeeId: number;
  transportId: number;
}
export interface FreightType {
  chargeType: number;
  createTime: string;
  hasFreeCondition: number;
  isFreeFee: number;
  shopId: number;
  transName: string;
  transfeeFrees: transfeeFreesType[];
  transfees: transfeesType[];
  transportId: number;
}
export interface FreightResType {
  current: number;
  pages: number;
  records: FreightType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": number,
	"pages": number,
	"records": [
		{
			"chargeType": number,
			"createTime": string,
			"hasFreeCondition": number,
			"isFreeFee": number,
			"shopId": number,
			"transName": string,
			"transfeeFrees": [
				{
					"amount": number,
					"freeCityList": [
						{
							"areaId": number,
							"areaName": string,
							"areas": [
								{
									"areaId": number,
									"areaName": string,
									"areas": [
										{}
									],
									"level": number,
									"parentId": number
								}
							],
							"level": number,
							"parentId": number
						}
					],
					"freeType": number,
					"piece": number,
					"transfeeFreeId": number,
					"transportId": number
				}
			],
			"transfees": [
				{
					"cityList": [
						{}
					],
					"continuousFee": number,
					"continuousPiece": number,
					"firstFee": number,
					"firstPiece": number,
					"transfeeId": number,
					"transportId": number
				}
			],
			"transportId": number
		}
	],
	"searchCount": true,
	"size": number,
	"total": 0
}
*/
