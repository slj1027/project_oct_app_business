import { tableType } from "@/types";
export interface HotReq extends tableType {
  content?: string;
  status?: number;
  title?: string;
}

export interface HotType {
  content: string;
  hotSearchId?: number;
  recDate?: string;
  seq: number;
  shopId?: number;
  status: number;
  title: string;
}
export interface HotResType {
  current: number;
  pages: number;
  records: HotType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": 0,
	"pages": 0,
	"records": [
		{
			"content": "",
			"hotSearchId": 0,
			"recDate": "",
			"seq": 0,
			"shopId": 0,
			"status": 0,
			"title": ""
		}
	],
	"searchCount": true,
	"size": 0,
	"total": 0
}
*/
