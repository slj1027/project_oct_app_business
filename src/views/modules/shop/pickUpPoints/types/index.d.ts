import { tableType } from "@/types";
export interface PickUpReq extends tableType {
  addrName?: string;
}
export interface PickUpType {
  addr: string;
  addrId?: undefined | number;
  addrName: string;
  area: string;
  areaId: undefined | number;
  city: string;
  cityId: undefined | number;
  mobile: string;
  province: string;
  provinceId: undefined | number;
  shopId?: number;
}
export interface PickUpResType {
  current: number;
  pages: number;
  records: PickUpType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": 0,
	"pages": 0,
	"records": [
		{
			"addr": "",
			"addrId": 0,
			"addrName": "",
			"area": "",
			"areaId": 0,
			"city": "",
			"cityId": 0,
			"mobile": "",
			"province": "",
			"provinceId": 0,
			"shopId": 0
		}
	],
	"searchCount": true,
	"size": 0,
	"total": 0
}
*/
