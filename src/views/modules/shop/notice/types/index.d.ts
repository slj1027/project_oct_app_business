import { tableType } from "@/types";
export interface NoticeReq extends tableType {
  content?: string;
  id?: number;
  isTop?: number;
  publishTime?: string;
  shopId?: number;
  status?: number;
  title?: string;
  updateTime?: string;
}

export interface NoticeType {
  content: string;
  id?: number;
  isTop: number;
  publishTime?: string;
  shopId?: number;
  status: number;
  title: string;
  updateTime?: string;
}
export interface NoticeResType {
  current: number;
  pages: number;
  records: NoticeType[];
  searchCount: true;
  size: number;
  total: number;
}
/* 
{
	"current": 0,
	"pages": 0,
	"records": [
		{
			"content": "",
			"id": 0,
			"isTop": 0,
			"publishTime": "",
			"shopId": 0,
			"status": 0,
			"title": "",
			"updateTime": ""
		}
	],
	"searchCount": true,
	"size": 0,
	"total": 0
}
*/
