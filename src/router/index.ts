import { createRouter, createWebHistory } from "vue-router";
import type { RouteRecordRaw } from "vue-router";
import configRoute from "@/utils/configRoute";
import storage from "@/utils/storage";
import { getNav } from "@/server";
import { navType } from "@/types";
// import storage from "@/utils/storage";
const routes: RouteRecordRaw[] = [
  { path: "/", redirect: "/login" },
  {
    path: "/home",
    name: "home",
    component: async () => await import("@/views/common/home/index.vue"),
    children: [
      {
        path: "/home/index",
        name: "index",
        component: async () => await import("@/views/common/index/index.vue"),
        meta: { title: "首页" },
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: async () => await import("@/views/common/login/index.vue"),
  },
  {
    path: "/:pathMatch(.*)",
    name: "404",
    component: async () => await import("@/views/common/404/index.vue"),
  },
];

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHistory(),
  routes, // `routes: routes` 的缩写
});
let load = 0;
router.beforeEach(async (to, from, next) => {
  if (storage.getCache("token")) {
    let { data } = (await getNav()) as { data: navType };
    storage.setCache("auth", data);
    let menuList = storage.getCache("auth").menuList;
    storage.setCache("nav", menuList);
    if (
      to.path === "/login" ||
      to.path === "/" ||
      to.path === "/home" ||
      to.path === "/home/index"
    ) {
      next();
    } else {
      if (load === 0 && to.path !== "/login") {
        configRoute(menuList).forEach((item) => {
          router.addRoute("home", item);
        });
        load++;
        next({ ...to, replace: true });
      } else {
        next();
      }
    }
  } else {
    if (to.path === "/login" || to.path === "/") {
      next();
    } else {
      next({ path: "/" });
    }
  }
});
export default router;

/*  {
        path: "/home/grouping",
        name: "grouping",
        component: async () =>
          await import("@/views/modules/product/grouping/index.vue"),
        meta: { title: "分组管理" },
      },
      {
        path: "/home/products",
        name: "products",
        component: async () =>
          await import("@/views/modules/product/products/index.vue"),
        meta: { title: "产品管理" },
      },
      {
        path: "/home/category",
        name: "category",
        component: async () =>
          await import("@/views/modules/product/category/index.vue"),
        meta: { title: "分类管理" },
      },
      {
        path: "/home/comment",
        name: "comment",
        component: async () =>
          await import("@/views/modules/product/comment/index.vue"),
        meta: { title: "评论管理" },
      },
      {
        path: "/home/specs",
        name: "specs",
        component: async () =>
          await import("@/views/modules/product/specs/index.vue"),
        meta: { title: "规格管理" },
      },
      {
        path: "/home/notice",
        name: "notice",
        component: async () =>
          await import("@/views/modules/shop/notice/index.vue"),
        meta: { title: "公告管理" },
      },
      {
        path: "/home/hot",
        name: "hot",
        component: async () =>
          await import("@/views/modules/shop/hot/index.vue"),
        meta: { title: "热搜管理" },
      },
      {
        path: "/home/swiper",
        name: "swiper",
        component: async () =>
          await import("@/views/modules/shop/swiper/index.vue"),
        meta: { title: "轮播图管理" },
      },
      {
        path: "/home/freight",
        name: "freight",
        component: async () =>
          await import("@/views/modules/shop/freight/index.vue"),
        meta: { title: "运费模板" },
      },
      {
        path: "/home/pickUpPoints",
        name: "pickUpPoints",
        component: async () =>
          await import("@/views/modules/shop/pickUpPoints/index.vue"),
        meta: { title: "自提点管理" },
      },
      {
        path: "/home/member",
        name: "member",
        component: async () => await import("@/views/modules/member/index.vue"),
        meta: { title: "会员管理" },
      },
      {
        path: "/home/orders",
        name: "orders",
        component: async () => await import("@/views/modules/orders/index.vue"),
        meta: { title: "订单管理" },
      },
      {
        path: "/home/address",
        name: "address",
        component: async () =>
          await import("@/views/modules/system/address/index.vue"),
        meta: { title: "地址管理" },
      },
      {
        path: "/home/admin",
        name: "admin",
        component: async () =>
          await import("@/views/modules/system/admin/index.vue"),
        meta: { title: "管理员列表" },
      },
      {
        path: "/home/role",
        name: "role",
        component: async () =>
          await import("@/views/modules/system/role/index.vue"),
        meta: { title: "角色管理" },
      },
      {
        path: "/home/menu",
        name: "menu",
        component: async () =>
          await import("@/views/modules/system/menu/index.vue"),
        meta: { title: "菜单管理" },
      },
      {
        path: "/home/timing",
        name: "timing",
        component: async () =>
          await import("@/views/modules/system/timing/index.vue"),
        meta: { title: "定时任务" },
      },
      {
        path: "/home/argument",
        name: "argument",
        component: async () =>
          await import("@/views/modules/system/argument/index.vue"),
        meta: { title: "参数管理" },
      },
      {
        path: "/home/system",
        name: "system",
        component: async () =>
          await import("@/views/modules/system/system/index.vue"),
        meta: { title: "系统日志" },
      }, */
