const routes = [
  {
    path: "/prod/prodTag",
    name: "prod:prodTag",
    component: async () =>
      await import("@/views/modules/product/grouping/index.vue"),
    meta: { title: "分组管理" },
  },
  {
    path: "/prod/prodList",
    name: "prod:prodList",
    component: async () =>
      await import("@/views/modules/product/products/index.vue"),
    meta: { title: "产品管理" },
  },
  {
    path: "/prod/category",
    name: "prod:category",
    component: async () =>
      await import("@/views/modules/product/category/index.vue"),
    meta: { title: "分类管理" },
  },
  {
    path: "/prod/prodComm",
    name: "prod:prodComm",
    component: async () =>
      await import("@/views/modules/product/comment/index.vue"),
    meta: { title: "评论管理" },
  },
  {
    path: "/prod/spec",
    name: "prod:spec",
    component: async () =>
      await import("@/views/modules/product/specs/index.vue"),
    meta: { title: "规格管理" },
  },
  {
    path: "/shop/notice",
    name: "shop:notice",
    component: async () =>
      await import("@/views/modules/shop/notice/index.vue"),
    meta: { title: "公告管理" },
  },
  {
    path: "/shop/hotSearch",
    name: "shop:hotSearch",
    component: async () => await import("@/views/modules/shop/hot/index.vue"),
    meta: { title: "热搜管理" },
  },
  {
    path: "/admin/indexImg",
    name: "shop:indexImg",
    component: async () =>
      await import("@/views/modules/shop/swiper/index.vue"),
    meta: { title: "轮播图管理" },
  },
  {
    path: "/shop/transport",
    name: "shop:transport",
    component: async () =>
      await import("@/views/modules/shop/freight/index.vue"),
    meta: { title: "运费模板" },
  },
  {
    path: "/shop/pickAddr",
    name: "shop:pickAddr",
    component: async () =>
      await import("@/views/modules/shop/pickUpPoints/index.vue"),
    meta: { title: "自提点管理" },
  },
  {
    path: "/user/user",
    name: "user:user",
    component: async () => await import("@/views/modules/member/index.vue"),
    meta: { title: "会员管理" },
  },
  {
    path: "/order/order",
    name: "order:order",
    component: async () => await import("@/views/modules/orders/index.vue"),
    meta: { title: "订单管理" },
  },
  {
    path: "/sys/area",
    name: "sys:area",
    component: async () =>
      await import("@/views/modules/system/address/index.vue"),
    meta: { title: "地址管理" },
  },
  {
    path: "/sys/user",
    name: "sys:user",
    component: async () =>
      await import("@/views/modules/system/admin/index.vue"),
    meta: { title: "管理员列表" },
  },
  {
    path: "/sys/role",
    name: "sys:role",
    component: async () =>
      await import("@/views/modules/system/role/index.vue"),
    meta: { title: "角色管理" },
  },
  {
    path: "/sys/menu",
    name: "sys:menu",
    component: async () =>
      await import("@/views/modules/system/menu/index.vue"),
    meta: { title: "菜单管理" },
  },
  {
    path: "/sys/schedule",
    name: "sys:schedule",
    component: async () =>
      await import("@/views/modules/system/timing/index.vue"),
    meta: { title: "定时任务" },
  },
  {
    path: "/sys/config",
    name: "sys:config",
    component: async () =>
      await import("@/views/modules/system/argument/index.vue"),
    meta: { title: "参数管理" },
  },
  {
    path: "/sys/log",
    name: "sys:log:page",
    component: async () =>
      await import("@/views/modules/system/system/index.vue"),
    meta: { title: "系统日志" },
  },
];
export default routes;
