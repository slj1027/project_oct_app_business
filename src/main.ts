import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import ElementPlus from "element-plus";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import { createPinia } from "pinia";
import "element-plus/dist/index.css";
import "./style.css";
import { useStore } from "@/store";
import request from "./utils/request";
import {
  Shop,
  List,
  Setting,
  User,
  UserFilled,
  Location,
  Medal,
  Timer,
  Coin,
  Document,
} from "@element-plus/icons-vue";
const pinia = createPinia();
const app = createApp(App).use(pinia).use(ElementPlus);
const store = useStore();
app.config.globalProperties.$request = request;
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
try {
  store.getMenu();
} finally {
  app.use(router).mount("#app");
}
app.component("Admin", UserFilled);
app.component("Store", Shop);
app.component("Vip", User);
app.component("Order", List);
app.component("Dangdifill", Location);
app.component("Role", Medal);
app.component("Job", Timer);
app.component("System", Setting);
app.component("Config", Coin);
app.component("Log", Document);
