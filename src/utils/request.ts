import axios, { AxiosRequestConfig, AxiosInstance } from "axios";
import { ElMessage, ElLoading } from "element-plus";
import { LoadingInstance } from "element-plus/lib/components/loading/src/loading";
import LocalSto from "./storage";
let DEFAULT_LOADING = true;
let whiteList = ["/api/captcha.jpg", "/api/login?grant_type=admin"];
interface HttpRequestConfig extends AxiosRequestConfig {
  isShowLoading?: boolean;
  isLoading?: LoadingInstance;
}
class HttpRequest {
  instance: AxiosInstance;
  isShowLoading: boolean;
  isLoading?: LoadingInstance;
  constructor(config: HttpRequestConfig) {
    this.instance = axios.create(config);
    this.isShowLoading = config.isShowLoading || DEFAULT_LOADING;
    this.instance.interceptors.request.use(
      (reqConfig) => {
        if (this.isShowLoading) {
          this.isLoading = ElLoading.service({
            lock: true,
            text: "正在请求",
            background: "rgba(0, 0, 0, 0.3)",
          });
        }
        this.isShowLoading = false;
        if (!whiteList.includes(reqConfig.url as string)) {
          reqConfig.headers = {
            ...reqConfig.headers,
            Authorization: LocalSto.getCache("token"),
          };
        }
        return {
          ...reqConfig,
        };
      },
      async (error) => {
        // 对请求错误做些什么
        return await Promise.reject(error);
      }
    );
    this.instance.interceptors.response.use(
      (response) => {
        this.isLoading?.close();

        return response;
      },
      async (error) => {
        this.isLoading?.close();
        // 对请求错误做些什么
        const { status, data } = error.response;

        switch (status) {
          case 400:
            ElMessage.error(data);
            break;
          case 401:
            ElMessage.error("没有访问权限, 请重新登录");
            window.location.href = "/login";
            break;
          case 404:
            ElMessage.error("请求错误, 找不到该请求");
            break;
          case 500:
            ElMessage.error("服务器出错了");
            break;
        }
        return await Promise.reject(error);
      }
    );
  }

  request<T>(config: HttpRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      this.instance
        .request<any, T>(config)
        .then((res) => {
          this.isShowLoading = DEFAULT_LOADING;
          resolve(res);
        })
        .catch((err) => {
          this.isShowLoading = DEFAULT_LOADING;
          reject(err);
          return err;
        });
    });
  }

  get<T>(config: HttpRequestConfig): Promise<T> {
    return this.request<T>({
      ...config,
      method: "GET",
    });
  }

  post<T>(config: HttpRequestConfig): Promise<T> {
    return this.request<T>({
      ...config,
      method: "post",
    });
  }

  put<T>(config: HttpRequestConfig): Promise<T> {
    return this.request<T>({
      ...config,
      method: "put",
    });
  }

  delete<T>(config: HttpRequestConfig): Promise<T> {
    return this.request<T>({
      ...config,
      method: "delete",
    });
  }
}

export default HttpRequest;
