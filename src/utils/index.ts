import HttpRequest from "./request";
import { AddressType, RenderAddress } from "@/types/address";
export const request = new HttpRequest({
  timeout: 10000,
});
export function treeDataTranslate(data: AddressType[]) {
  let res: RenderAddress[] = data.filter((item: AddressType) => !item.parentId);
  res.forEach((item) => {
    item.children = data.filter((a: AddressType) => a.parentId === item.areaId);
    item.children.forEach((v: RenderAddress) => {
      v.children = data.filter((a: AddressType) => a.parentId === v.areaId);
    });
  });
  console.log(res);
  return res;
}
