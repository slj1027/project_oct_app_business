import { RouteRecordRaw } from "vue-router";

const configRoute = (menuList: any[]) => {
  let routes: RouteRecordRaw[] = [];
  let allRoutes: RouteRecordRaw[] = [];

  const routeFile = import.meta.globEager("@/router/modules/*.ts");
  allRoutes = Object.keys(routeFile).reduce((pre: any, next: string) => {
    pre.push(...(routeFile[next] as { default: RouteRecordRaw[] }).default);
    return pre;
  }, []);

  const changeRoutes = (menu: any) => {
    for (const item of menu) {
      if (item.type === 1) {
        const route = allRoutes.find((val) => val.path === "/" + item.url);
        if (route) routes.push(route);
      } else {
        item.list && changeRoutes(item.list);
      }
    }
  };
  changeRoutes(menuList);
  return routes;
};

export default configRoute;
