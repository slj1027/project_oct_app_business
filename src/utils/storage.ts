class LocalSto {
  setCache(key: string, value: any) {
    if (typeof value !== "string") {
      window.localStorage.setItem(key, JSON.stringify(value));
    } else {
      window.localStorage.setItem(key, value);
    }
  }

  getCache(key: string) {
    let value = window.localStorage.getItem(key);
    if (value) {
      try {
        return JSON.parse(value);
      } catch (error) {
        return value;
      }
    }
    return null;
  }

  clearCache() {
    window.localStorage.clear();
  }

  removeCache(key: string) {
    window.localStorage.removeItem(key);
  }
}

export default new LocalSto();
