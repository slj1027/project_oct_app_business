import { defineStore } from "pinia";
import { userInfoType } from "@/types";
import storage from "@/utils/storage";
import { getCaptcha, userLogin, getUserInfo } from "@/server";
import { uuid } from "vue-uuid";
import configRoute from "@/utils/configRoute";
import route from "@/router";

interface stateType {
  nav: { path: string; name: string }[];
  active: string;
  captchaPath: string;
  uuid: string;
}
interface toLoginType {
  userName: string;
  password: string;
  uuid: string;
  captcha: string;
}
// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
export const useStore = defineStore("main", {
  // other options...
  state: (): stateType => {
    return {
      // 所有这些属性都将自动推断其类型

      nav: localStorage.getItem("headNav")
        ? JSON.parse(localStorage.getItem("headNav") as string)
        : [],
      active: localStorage.getItem("active")
        ? (localStorage.getItem("active") as string)
        : "",
      captchaPath: "",
      uuid: uuid.v4(),
    };
  },
  getters: {
    // doubleCount: (state) => {},
  },
  actions: {
    async getCaptchaData() {
      let data = (await getCaptcha({
        uuid: this.uuid,
      })) as { data: ArrayBuffer };
      this.captchaPath =
        "data: image/png;base64," +
        btoa(
          new Uint8Array(data.data).reduce(
            (data, byte) => data + String.fromCharCode(byte),
            ""
          )
        );
    },
    async toLogin(payload: toLoginType) {
      try {
        let { data } = (await userLogin({
          principal: payload.userName,
          credentials: payload.password,
          sessionUUID: payload.uuid,
          imageCode: payload.captcha,
        })) as {
          data: { token_type: string; access_token: string; userId: number };
        };
        if (data) {
          localStorage.setItem("token", data.token_type + data.access_token);
          let { data: userInfo } = (await getUserInfo()) as {
            data: userInfoType;
          };
          storage.setCache("userInfo", userInfo);
          route.push("/home/index");
          // this.getMenu();
        }
      } catch (error) {
        this.getCaptchaData();
      }
    },
    async getMenu() {
      let auth = storage.getCache("auth");
      if (auth) {
        storage.setCache("nav", auth.menuList);
        configRoute(auth.menuList).forEach((item) => {
          route.addRoute("home", item);
        });
      }
    },
  },
});
