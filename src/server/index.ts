import { EditResPsw, loginType } from "@/types/index";
import { request } from "@/utils";
// 获取验证码
export const getCaptcha = (params: { uuid: string }) =>
  request.get({
    url: "/api/captcha.jpg",
    method: "get",
    params,
    responseType: "arraybuffer",
    isShowLoading: false,
  });
// 登录
export const userLogin = (data: loginType) =>
  request.post({
    url: "/api/login?grant_type=admin",
    method: "post",
    data,
  });
// 退出登录
export const userLogout = () =>
  request.post({
    url: "/api/sys/logout",
    method: "post",
    isShowLoading: false,
  });
// 获取用户信息
export const getUserInfo = () =>
  request.get({
    url: "/api/sys/user/info",
    method: "get",
    isShowLoading: false,
  });
// 获取权限路由及操作
export const getNav = () =>
  request.get({
    url: "/api/sys/menu/nav",
    method: "get",
    isShowLoading: false,
  });
// 修改密码
export const editPassword = (data: EditResPsw) =>
  request.post({
    url: "/api/sys/user/password",
    method: "post",
    data,
    isShowLoading: false,
  });
