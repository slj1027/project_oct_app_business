import { tableType } from "@/types";
import { request } from "@/utils";
import { editRoleType } from "@/views/role/types";
interface resType extends tableType {
  roleName?: string;
}
export const getRole = (params: resType) =>
  request.get({
    url: "/api/sys/role/page",
    method: "get",
    params,
  });
export const getRoleList = () =>
  request.get({
    url: "/api/sys/role/list",
    method: "get",
  });
export const getRoleInfo = (id: number) =>
  request.get({
    url: `/api/sys/role/info/${id}`,
    method: "get",
  });
export const saveRole = (data: editRoleType) =>
  request.post({
    url: "/api/sys/role",
    data,
  });
export const editRole = (data: editRoleType) =>
  request.put({
    url: "/api/sys/role",
    data,
  });
export const delRole = (data: number[]) =>
  request.delete({
    url: "/api/sys/role",
    data,
  });
