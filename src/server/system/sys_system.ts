import { tableType } from "@/types";
import { request } from "@/utils";
interface resType extends tableType {
  username?: string;
  operation?: string;
}
export const getSystem = (params: resType) =>
  request.get({
    url: "/api/sys/log/page",
    method: "get",
    params,
  });
