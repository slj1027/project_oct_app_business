// 地址管理

import { request } from "@/utils";

export const getAddress = (params: any) =>
  request.get({
    url: "/api/admin/area/list",
    method: "get",
    params,
  });
export const getAddressInfo = (id: number) =>
  request.get({
    url: `/api/admin/area/info/${id}`,
    method: "get",
  });
export const editAddress = (data: any) =>
  request.put({
    url: "/api/admin/area",
    method: "put",
    data,
  });
export const delAddress = (id: number) =>
  request.delete({
    url: `/api/admin/area/${id}`,
    method: "delete",
  });
export const saveAddress = (data: any) =>
  request.post({
    url: "/api/admin/area",
    method: "post",
    data,
  });
