import { tableType } from "@/types";
import { request } from "@/utils";
import { editAdminType } from "@/views/modules/system/admin/types";
interface resType extends tableType {
  username?: string;
}
export const getAdmin = (params: resType) =>
  request.get({
    url: "/api/sys/user/page",
    params,
  });
export const saveAdmin = (data: editAdminType) =>
  request.post({
    url: "/api/sys/user",
    data,
  });
export const editAdmin = (data: editAdminType) =>
  request.put({
    url: "/api/sys/user",
    data,
  });
export const delAdmin = (data: number[]) =>
  request.delete({
    url: "/api/sys/user",
    data,
  });
export const getAdminInfo = (params: number) =>
  request.get({
    url: `/api/sys/user/info/${params}`,
  });
