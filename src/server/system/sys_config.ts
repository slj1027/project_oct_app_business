// 系统管理 - 参数管理

import { tableType } from "@/types";
import { request } from "@/utils";
import { configType } from "@/views/modules/system/argument/types";
interface GetConfigParams extends tableType {
  paramKey?: string;
}
// 获取列表
export const getConfig = (params: GetConfigParams) =>
  request.get({
    url: "/api/sys/config/page",
    method: "get",
    params,
  });
export const getConfigInfo = (id: number) =>
  request.get({
    url: `/api/sys/config/info/${id}`,
    method: "get",
  });
// 编辑
export const editConfig = (data: configType) =>
  request.put({
    url: "/api/sys/config",
    method: "put",
    data,
  });
// 新增
export const saveConfig = (data: configType) =>
  request.post({
    url: "/api/sys/config",
    method: "post",
    data,
  });
// 删除
export const delConfig = (data: number[]) =>
  request.delete({
    url: "/api/sys/config",
    method: "delete",
    data,
  });
