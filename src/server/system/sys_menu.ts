import { request } from "@/utils";
import { editMenuType } from "@/views/modules/system/Menu/types";
export const getMenu = () =>
  request.get({
    url: "/api/sys/menu/table",
  });
export const saveMenu = (data: editMenuType) =>
  request.post({
    url: "/api/sys/menu",
    data,
  });
export const editMenu = (data: editMenuType) =>
  request.put({
    url: "/api/sys/menu",
    data,
  });
export const delMenu = (data: number) =>
  request.delete({
    url: `/api/sys/menu/${data}`,
  });
export const getMenuInfo = (params: number) =>
  request.get({
    url: `/api/sys/menu/info/${params}`,
  });
