import { tableType } from "@/types";
import { request } from "@/utils";
import { editScheduleType } from "@/views/modules/system/timing/types";
interface resType extends tableType {
  username?: string;
}
export const getSchedule = (params: resType) =>
  request.get({
    url: "/api/sys/schedule/page",
    params,
  });
export const saveSchedule = (data: editScheduleType) =>
  request.post({
    url: "/api/sys/schedule",
    data,
  });
export const editSchedule = (data: editScheduleType) =>
  request.put({
    url: "/api/sys/schedule",
    data,
  });
export const delSchedule = (data: number[]) =>
  request.delete({
    url: "/api/sys/schedule",
    data,
  });
export const getScheduleInfo = (params: number) =>
  request.get({
    url: `/api/sys/schedule/info/${params}`,
  });
export const setSchedulePause = (data: number[]) =>
  request.post({
    url: "/api/sys/schedule/pause",
    data,
  });
export const setScheduleRun = (data: number[]) =>
  request.post({
    url: "/api/sys/schedule/run",
    data,
  });
export const setScheduleResume = (data: number[]) =>
  request.post({
    url: "/api/sys/schedule/resume",
    data,
  });
export const getScheduleLog = (params: { jobId: number }) =>
  request.get({
    url: "/api/sys/scheduleLog/page",
    params,
  });
