// 订单管理
import { request } from "@/utils";
import { paramsSearch } from "../../views/modules/orders/types/search";
import { orderType } from "../../views/modules/orders/types/index";
// 获取订单数据
export const getOrder = (params: any) =>
  request.get({
    url: "/api/order/order/page",
    method: "get",
    params,
  });
// 搜索
export const getSearch = (params: paramsSearch) =>
  request.get<orderType>({
    url: "/api/order/order/page",
    method: "get",
    params,
  });
// 点击修改
export const getEdit = (params: any) =>
  request.get({
    url: `/api/order/order/orderInfo/${params.id}`,
    method: "get",
    params,
  });
