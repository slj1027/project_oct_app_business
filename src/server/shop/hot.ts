// 公告列表

import { request } from "@/utils";
import { HotReq, HotType } from "@/views/modules/shop/hot/types";
// 获取公告列表
export const getHot = (params: HotReq) =>
  request.get({
    url: "/api/admin/hotSearch/page",
    method: "get",
    params,
  });
// 单个公告
export const getHotItem = (id: number) =>
  request.get({
    url: `/api/admin/hotSearch/info/${id}`,
    method: "get",
  });
// 添加
export const saveHot = (data: HotType) =>
  request.post({
    url: "/api/admin/hotSearch",
    method: "post",
    data,
  });
// 编辑
export const editHot = (data: HotType) =>
  request.put({
    url: "/api/admin/hotSearch",
    method: "put",
    data,
  });
// 删除
export const delHot = (ids: number[]) =>
  request.delete({
    url: "/api/admin/hotSearch",
    method: "delete",
    data: ids,
  });
