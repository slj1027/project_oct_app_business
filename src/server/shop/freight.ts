// 运费列表

import { request } from "@/utils";
import { FreightReq, FreightType } from "@/views/modules/shop/freight/types";
// 获取运费列表
export const getFreight = (params: FreightReq) =>
  request.get({
    url: "/api/shop/transport/page",
    method: "get",
    params,
  });
// 单个运费
export const getFreightItem = (id: number) =>
  request.get({
    url: `/api/shop/transport/info/${id}`,
    method: "get",
  });
// 添加
export const saveFreight = (data: FreightType) =>
  request.post({
    url: "/api/shop/transport",
    method: "post",
    data,
  });
// 编辑
export const editFreight = (data: FreightType) =>
  request.put({
    url: "/api/shop/transport",
    method: "put",
    data,
  });
// 删除
export const delFreight = (ids: number[]) =>
  request.delete({
    url: "/api/shop/transport",
    method: "delete",
    data: ids,
  });
