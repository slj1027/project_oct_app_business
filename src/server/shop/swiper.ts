// 公告列表

import { request } from "@/utils";
import { SwiperReq, SwiperType } from "@/views/modules/shop/swiper/types";
// 获取公告列表
export const getSwiper = (params: SwiperReq) =>
  request.get({
    url: "/api/admin/indexImg/page",
    method: "get",
    params,
  });
// 单个公告
export const getSwiperItem = (id: number) =>
  request.get({
    url: `/api/admin/indexImg/info/${id}`,
    method: "get",
  });
// 添加
export const saveSwiper = (data: SwiperType) =>
  request.post({
    url: "/api/admin/indexImg",
    method: "post",
    data,
  });
// 编辑
export const editSwiper = (data: SwiperType) =>
  request.put({
    url: "/api/admin/indexImg",
    method: "put",
    data,
  });
// 删除
export const delSwiper = (data: number[]) =>
  request.delete({
    url: "/api/admin/indexImg",
    method: "delete",
    data,
  });
