// 自提点列表

import { request } from "@/utils";
import { PickUpReq, PickUpType } from "@/views/modules/shop/pickUpPoints/types";
// 获取自提点列表
export const getPickUp = (params: PickUpReq) =>
  request.get({
    url: "/api/shop/pickAddr/page",
    method: "get",
    params,
  });
// 单个自提点
export const getPickUpItem = (id: number) =>
  request.get({
    url: `/api/shop/pickAddr/info/${id}`,
    method: "get",
  });
// 添加
export const savePickUp = (data: PickUpType) =>
  request.post({
    url: "/api/shop/pickAddr",
    method: "post",
    data,
  });
// 编辑
export const editPickUp = (data: PickUpType) =>
  request.put({
    url: "/api/shop/pickAddr",
    method: "put",
    data,
  });
// 删除
export const delPickUp = (ids: number[]) =>
  request.delete({
    url: "/api/shop/pickAddr",
    method: "delete",
    data: ids,
  });
export const getAddress = (params: number) =>
  request.get({
    url: "/api/admin/area/listByPid",
    method: "get",
    params: {
      pid: params,
    },
  });
