// 公告列表

import { request } from "@/utils";
import { NoticeReq, NoticeType } from "@/views/modules/shop/notice/types";
// 获取公告列表
export const getNotice = (params: NoticeReq) =>
  request.get({
    url: "/api/shop/notice/page",
    method: "get",
    params,
  });
// 单个公告
export const getNoticeItem = (id: number) =>
  request.get({
    url: `/api/shop/notice/info/${id}`,
    method: "get",
  });
// 添加
export const saveNotice = (data: NoticeType) =>
  request.post({
    url: "/api/shop/notice",
    method: "post",
    data,
  });
// 编辑
export const editNotice = (data: NoticeType) =>
  request.put({
    url: "/api/shop/notice",
    method: "put",
    data,
  });
// 删除
export const delNotice = (id: number) =>
  request.delete({
    url: `/api/shop/notice/${id}`,
    method: "delete",
  });
