import { MenuType } from "@/views/menu/types/index";
// 表格分页传参
export interface tableType {
  current: number;
  size: number;
}
// 表格页面传参
export interface ColumnType {
  label: string;
  props: string;
  width?: number | string;
  type?: string;
  fixed?: string;
  actions?: {
    name: string;
    type?: string;
    icon?: string;
    event: (data: any) => void;
  }[];
  render?: (params: any) => string;
  rounce?: string;
  key?: number;
}
// 登录接口传参
export interface loginType {
  principal: string;
  credentials: string;
  sessionUUID: string;
  imageCode: string;
}
export interface userInfoType {
  createTime: string;
  email: string;
  mobile: string;
  roleIdList: number[] | null;
  shopId: number;
  status: number;
  userId: number;
  username: string;
}
export interface authoritiesType {
  authority: string;
}
export interface navType {
  authorities: authoritiesType[];
  menuList: MenuType[];
}
export interface EditResPsw {
  password: string;
  newPassword: string;
}
