import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";
import ESlint from "vite-plugin-eslint";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    ESlint({
      exclude: ["./node_modules"],
      include: [
        "src/*.ts",
        "src/*.vue",
        "src/**/*.js",
        "src/**/*.vue",
        "src/**/*.jsx",
        "src/**/*.ts",
      ],
      cache: false,
    }),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
  server: {
    proxy: {
      "/api": {
        target: "https://bjwz.bwie.com/mall4w",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
