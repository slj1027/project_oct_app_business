module.exports = {
  extends: ["@commitlint/config-conventional"],
  rules: {
    "type-enum": [
      2,
      "always",
      ["feat", "bug", "fix", "docs", "style", "refactor", "test", "revert"],
    ],
  },
};
